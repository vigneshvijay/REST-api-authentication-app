const router = require('express').Router();
//user registration function import 
const {userRegister, userLogin} = require('../utils/Auth');


//test route
router.post('/test',(req,res) => {
    res.send(JSON.stringify(req.body))
});
/**
 * @DESC routes to register the user (ADMIN,SUPER-ADMIN,USER)
 */
//register user route
router.post('/register-user', async (req,res) => {
    // console.log("lalalla===>",req);
    await userRegister(req.body,"user",res);
});

//register admin route
router.post('/register-admin', async (req,res) => {
    await userRegister(req.body,'admin',res);
});

//register super Admin route
router.post('/register-super-admin', async (req,res) => {
    await userRegister(req.body,"super-admin",res);
});

/**
 * @DESC Login routes of the user (ADMIN,SUPER-ADMIN,USER)
 */

//login user route
router.post('/login-user', async (req,res) => {
    await userLogin(req.body,"user",res);
});

//login admin route
router.post('/login-admin', async (req,res) => {
    await userLogin(req.body,"admin",res);
});

//login super Admin route
router.post('/login-super-admin', async (req,res) => {
    await userLogin(req.body,"super-admin",res);
});

/**
 * @DESC Protected routes of the user (ADMIN,SUPER-ADMIN,USER)
 */

//Profile route 
router.get('profile', async (req,res) => {})

//protected user route
router.post('/user-protected', async (req,res) => {});
//protected admin route
router.post('/admin-protected', async (req,res) => {});
//protected super Admin route
router.post('/super-admin-protected', async (req,res) => {});





module.exports = router;