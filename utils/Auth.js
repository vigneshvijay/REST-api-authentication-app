const bcrypt = require('bcryptjs');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { SECRET } = require('../config/index');

/**
 * @DESC To register the user (ADMIN,SUPER_ADMIN,USER)
 */
const userRegister = async (userDets,role,res) => {

    try {
        // console.log(userDets,"role==>",role);
        //validate user
        let usernameNotTaken = await validateUsername(userDets.username);
        if(!usernameNotTaken){
            return res.status(400).json({
                message : `Username: ${userDets.username}is already taken.`,
                success : false
            });
        }
        //validate Email
        let emailNotRegistered = await validateEmail(userDets.email);
        if(!emailNotRegistered){
            return res.status(400).json({
                message : `Email : ${userDets.email} is already taken`,
                success : false
            })
        }

        //Pasword hashing
        const password = await bcrypt.hash(userDets.password,12);
        // console.log(password);
        //user creation 
        const newUser =  new User({
            ...userDets,
            password,
            role
        },);
        // console.log('new======>',newUser);
        await newUser.save();
        return res.status(201).json({
            message : "You are successfully registered. Please login now",
            success : true
        }); 
    } 
    catch (error) {
        //can implement logger
        console.log(error);
        return res.status(500).json({
            message : "Failed to create the account",
            success : false
        })
        
    }
    
    
};

/**
 * @DESC To login the user (ADMIN,SUPER_ADMIN,USER)
 */

const userLogin = async (userCreds,role,res) => {
    let { email,password } = userCreds;
    //check if username is in DB
    const user = await User.findOne({email});
    if(!user){
        return res.status(404).json({
            message : `Email address:${email}  not found. Invalid login credentials`,
            success : false
        });
    }
    //Check the role
    if(user.role != role){
        return res.status(403).json({
            message : `Please make sure you are ${role} for logging in`,
            success : false
        });
    }
    //user exists and trying to sign-in from the right portal
    //check password match
    let isMatch = await bcrypt.compare(password,user.password);
    if(isMatch){
        //signin success and issue token
        let token = jwt.sign(
            { user_id : user._id,
              role : user.role, 
              username : user.username,  
              email : user.email
            },
            SECRET,
            {expiresIn: "10 days"}
        );

        let result = {
            username : user.username,
            role : user.role,
            email : user.email,
            token,
            expiresIn : 200
        };

        return res.status(201).json({
            ...result,
            message : "You are successfully logged-in",
            success : true
        }); 



    } else{
        return res.status(403).json({
            message : `Incorrect password`,
            success : false
        });
    }

}

const validateUsername =async username => {
    let user = await User.findOne({ username });
    return user ? false : true;
};

const validateEmail = async email => {
    let user = await User.findOne({ email });
    return user ? false : true;
}



module.exports = {
    userRegister,
    userLogin
}