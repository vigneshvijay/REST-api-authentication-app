const cors = require('cors');
const express = require("express");
const {connect} = require('mongoose');
const {success,error} = require('consola');


// Bring in the app const 
const {DB,PORT} = require('./config');

// Initialize the app 
const app = express();

// Middleware 
app.use(cors());
app.use(express.urlencoded());
app.use(express.json());

//router Middleware
app.use('/api/user', require('./routes/users'));


const startApp = async () => {
   try {

    //Connection with DB
    await connect(DB,{
        useUnifiedTopology:true,
        useNewUrlParser:true,
        useFindAndModify:true
    });

    success({
        message : `DB connection establised on \n ${DB}`,
        badge : true
    });
    //Start listening to the server on PORT
    app.listen(PORT,() => 
        success({ message : `Server started on PORT ${PORT}`,badge:true})
    );
       
   } catch (err) {
    error({
        message : `Unable to connect to DB \n${err}`,
        badge : true   
   });
   startApp();
}
}

startApp();
