# Getting Started with REST-API role-based authentication app.

Soon after cloning... head over to the terminal and hit `npm install`(ensure that the terminal corresponds to the cloned directory).


## App insights

This is an expressJS based app employing MongoDB as the back-end.
Please note, this app supports role-based login(can be found under `routes > users.js`).


Reproducing the following steps will make this app work like a charm.

Step 1 : Go to the app directory, create a file named .env and ensure to furnish the following.

   ####  By default the DB(collection) path is as follows. you can replace DB(collection) with your desired name.
         ```
         APP_DB = mongodb://localhost:27017/DB    
         APP_SECRET = sdfsdfsdfsdf
         APP_PORT = 5000

         ```
   #### If the app fails to run in the above configuration, go to`config > index.js` and hard-code the values under `module.exports`.



Step 2 : You can run the app in two ways : go to the terminal and hit `npm start`
          or just run `nodemon` (if it is installed globally on your PC).
         
Step 3 : Headover to postman and make a post request to : `http://localhost:5000/api/user/test` 
         with some JSON values in the body. 
         If you receive what you've send, you can make sure that the app works.

Step 4 : Kindly visit `routes > users.js` to explore and edit the routes with roles defined there.

Step 5 : The saved data can be explored using `MongoDB Compass`.

Step 5 : Game Over..!


### About

This is a node app composed using expressJS framework. 
For any queries, feel free to reach me at `email : vigneshvj@yandex.com`


